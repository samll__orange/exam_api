# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

users = User.create([{
    username: 'mandy', 
    password: 'abc666', 
    name: '张曼迪'
}, 
{
    username: 'fire', 
    password: 'abc666', 
    name: '橙子'}
])

firms = Firm.create([{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长好长好长好长好长好长好长好长好长好长好长好长好长好长好长的地址呀',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '小米科技股份有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道',
    post_code: '100000',
    follow_user_id: 2
},
{
    name: '华为通信有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长地址呀',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长',
    post_code: '100000',
    follow_user_id: 2
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长呀',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长好长好',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道好长好长好长好长好长好长',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道',
    post_code: '100000',
    follow_user_id: 1
},
{
    name: '北京瑞达恒建筑咨询有限公司',
    phone: '010-27645688',
    address: '北京市西城区椿树街道',
    post_code: '100000',
    follow_user_id: 1
}])

Contact.create([{
    name: '玛丽',
    sex: 0,
    level: 1,
    phone: '322738231',
    email: '748327@qq.com',
    remark: '备注一下哈哈哈',
    firm_id: 1
}, 
{
    name: '佩奇',
    sex: 1,
    level: 0,
    phone: '322738231',
    email: '748327@qq.com',
    remark: '备注一下哈哈哈',
    firm_id: 1
},
{
    name: '汤姆',
    sex: 1,
    level: '',
    phone: '322738231',
    email: '748327@qq.com',
    remark: '备注一下哈哈哈',
    firm_id: 2
},
{
    name: '马克',
    sex: 1,
    level: '',
    phone: '322738231',
    email: '748327@qq.com',
    remark: '备注一下哈哈哈',
    firm_id: 2
},
{
    name: '梦琪',
    sex: 1,
    level: '',
    phone: '322738231',
    email: '748327@qq.com',
    remark: '备注一下哈哈哈',
    firm_id: 3
}])