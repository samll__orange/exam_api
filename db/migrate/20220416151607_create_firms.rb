class CreateFirms < ActiveRecord::Migration[7.0]
  def change
    create_table :firms do |t|
      t.string :name, comment: '公司名', null: false
      t.string :phone, comment: '公司电话'
      t.string :address, comment: '公司地址'
      t.string :post_code, comment: '邮政编码'
      t.integer :follow_user_id, comment: '跟进人'

      t.timestamps
    end
  end
end
