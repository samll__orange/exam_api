class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :username, comment: '用户名', null: false
      t.string :password, comment: '密码', null: false
      t.string :name, comment: '姓名'
      t.integer :sex, comment: '性别'
      t.integer :manager_id, comment: '上级'
      t.timestamps
    end
  end
end
