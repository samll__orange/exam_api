class CreateContacts < ActiveRecord::Migration[7.0]
  def change
    create_table :contacts do |t|
      t.string :name, comment: '姓名', null: false
      t.integer :sex, comment: '性别'
      t.string :department, comment: '部门'
      t.integer :level, comment: '重要性'
      t.string :phone, comment: '手机'
      t.string :mobile, comment: '电话'
      t.string :email, comment: '邮箱'
      t.text :remark, comment: '备注'
      t.timestamps
    end
  end
end
