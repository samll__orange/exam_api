class ContactApi < Grape::API
    format 'json'
 
  namespace :contact do

    desc '添加联系人'
    params do
        requires :firm_id, type: Integer
        requires :name, type: String
    end
 
    post 'add_contact' do
        puts '---djsjd---', params
        firm = Firm.find_by_id(params[:firm_id])
        return {status: 'failed', msg: '未查询到公司信息'} if firm.blank?

        create_info = Contact.create(params)
        return {status: 'failed', msg: '添加联系人失败'} if create_info.blank?
                
        {status: 'success', msg: '添加联系人成功'}
    end
 
    get 'hello' do
        [{name: 'hello user'},{password: 'hahah'}]
    end
  end
     
end