class FirmApi < Grape::API
    format 'json'
 
  namespace :firm do
    desc '公司列表'
    params do
        requires :page_size, type: Integer
        requires :current_page, type: Integer
    end

    get 'firm_list' do
        res_data = Firm.includes(:user).all.limit(params[:page_size]).offset((params[:current_page] - 1) * params[:page_size]).map{ |fir| {id: fir.id, name: fir.name, follow_user: fir.user.name, phone: fir.phone, address: fir.address, post_code: fir.post_code} }
        return {msg: '未查询到公司信息'} if res_data.blank?
        # 记录总数
        count = Firm.count
        {msg: 'success', data: {res_data: res_data, count: count}}
    end

    desc '公司详细信息'
    params do
        requires :firm_id, type: Integer
    end
 
    get 'firm_detail' do
        firm = Firm.includes(:user, :contacts).find_by_id(params[:firm_id])
        return {msg: '未查询到公司信息'} if firm.blank?
        
        res_data = {
            name: firm.name, 
            follow_user: firm.user.name, 
            phone: firm.phone, 
            address: firm.address, 
            post_code: firm.post_code,
            contacts: firm.contacts.map{ |cont| cont }
        }
        {msg: 'success', data: res_data}
    end
 
    get 'hello' do
        [{name: 'hello user'},{password: 'hahah'}]
    end
  end
     
end