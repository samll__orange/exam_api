class User < ApplicationRecord
    has_many :firms, class_name: "Firm", foreign_key: "follow_user_id"
end
