class Firm < ApplicationRecord
    has_many :contacts, class_name: "Contact", dependent: :destroy
    belongs_to :user, class_name: "User", foreign_key: "follow_user_id"
end
