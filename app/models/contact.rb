class Contact < ApplicationRecord
    belongs_to :firm, class_name: "Firm", foreign_key: "firm_id"
end
